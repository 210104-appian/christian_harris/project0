package user;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import dev.harris.util.CredentialsUtil;

public class CredentialsUtilTest {

    @Test(expected = NullPointerException.class)
    public void testPasswordIsValidNullArgument() {
	CredentialsUtil.passwordIsValid(null);
    }

    @Test
    public void testPasswordTooLong() {
	assertEquals(CredentialsUtil.passwordIsValid("Th!$sPas$wordIsT00Lo0o0o0ng"), false);
	assertEquals(CredentialsUtil
		.passwordIsValid("Th!sPas$wordIsWayyyyyyyyyyyyTooooo0oooooooLooooooooonnnnnnnnngggggggggg"), false);
	assertEquals(CredentialsUtil.passwordIsValid("Th!$Pas$wrdIsBar3lyTo0Long"), false);
    }

    @Test
    public void testPasswordTooShort() {
	assertEquals(CredentialsUtil.passwordIsValid(""), false);
	assertEquals(CredentialsUtil.passwordIsValid("a"), false);
	assertEquals(CredentialsUtil.passwordIsValid("s$7g"), false);
	assertEquals(CredentialsUtil.passwordIsValid("1d$j%lb"), false);
    }

    @Test
    public void testPasswordNoLetters() {
	assertEquals(CredentialsUtil.passwordIsValid("12$45%0#"), false);
	assertEquals(CredentialsUtil.passwordIsValid("1234%#&.09#4"), false);
	assertEquals(CredentialsUtil.passwordIsValid("!$345&#!64%%"), false);
    }

    @Test
    public void testPasswordNoNumbers() {
	assertEquals(CredentialsUtil.passwordIsValid("asdf%$sldls"), false);
	assertEquals(CredentialsUtil.passwordIsValid("oei&%sk!"), false);
	assertEquals(CredentialsUtil.passwordIsValid("#$!%dld#%sdl"), false);
    }

    @Test
    public void testPasswordNoValidSpecialCharacters() {
	assertEquals(CredentialsUtil.passwordIsValid("adfl39dl"), false);
	assertEquals(CredentialsUtil.passwordIsValid("sldkfj3k2ldlf9d"), false);
	assertEquals(CredentialsUtil.passwordIsValid("103ldlsl439dksl"), false);
    }

    @Test
    public void testPasswordInvalidCharacters() {
	assertEquals(CredentialsUtil.passwordIsValid("er43%!=6"), false);
	assertEquals(CredentialsUtil.passwordIsValid("asdf[%$321sdl"), false);
	assertEquals(CredentialsUtil.passwordIsValid("49fl%$sl,<!"), false);
    }

    @Test
    public void testPasswordIsValid() {
	assertEquals(CredentialsUtil.passwordIsValid("D!etC0ke"), true);
	assertEquals(CredentialsUtil.passwordIsValid("H#ppyB1rthD#y"), true);
	assertEquals(CredentialsUtil.passwordIsValid("IL0v3Pa$$w0rd$."), true);
	assertEquals(CredentialsUtil.passwordIsValid("Th$P$wdJstAbtT0LngBtNtQte"), true);
    }

    // --------------------------------------------------------------------------------------------------

    @Test(expected = NullPointerException.class)
    public void testUsernameIsValidNullArgument() {
	CredentialsUtil.usernameIsValid(null);
    }

    @Test
    public void testUsernameIsValidDoesNotBeginWithCharacter() {
	assertEquals(CredentialsUtil.usernameIsValid("1JohnStockton33"), false);
	assertEquals(CredentialsUtil.usernameIsValid(".IAmLegend45"), false);
	assertEquals(CredentialsUtil.usernameIsValid("_Stacy_Smith_"), false);
	assertEquals(CredentialsUtil.usernameIsValid("11111111"), false);
    }

    @Test
    public void testUsernameTooLong() {
	assertEquals(CredentialsUtil.usernameIsValid("IAmTheBestBankCustomerEver!!!"), false);
	assertEquals(CredentialsUtil.usernameIsValid("UsernameIsBarelyTooLong111"), false);
	assertEquals(CredentialsUtil.usernameIsValid("ThisUsernameIsWayyyyyyyyyyT0000000000Looooooooong"), false);
    }

    @Test
    public void testUsernameTooShort() {
	assertEquals(CredentialsUtil.usernameIsValid("1"), false);
	assertEquals(CredentialsUtil.usernameIsValid("abe"), false);
	assertEquals(CredentialsUtil.usernameIsValid("yo33"), false);
    }

    @Test
    public void testUsernameContainsIllegalCharacters() {
	assertEquals(CredentialsUtil.usernameIsValid("JohnDoe33*"), false);
	assertEquals(CredentialsUtil.usernameIsValid("Joey@gmail.com"), false);
	assertEquals(CredentialsUtil.usernameIsValid("Li$a;Anne"), false);
    }

    @Test
    public void testUsernameIsValid() {
	assertEquals(CredentialsUtil.usernameIsValid("cHarris"), true);
	assertEquals(CredentialsUtil.usernameIsValid("admin"), true);
	assertEquals(CredentialsUtil.usernameIsValid("Leo_Harris_Rules"), true);
    }

}
