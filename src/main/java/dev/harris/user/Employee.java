package dev.harris.user;

public class Employee extends User {

    public Employee(String username, String firstName, String lastName)
	    throws NullPointerException, IllegalArgumentException {
	super(username, firstName, lastName);
    }
}
