package dev.harris.user;

import java.util.ArrayList;

import dev.harris.account.Account;

public class Customer extends User {
    private final ArrayList<Account> accounts;

    public Customer(String username, String firstName, String lastName)
	    throws NullPointerException, IllegalArgumentException {
	super(username, firstName, lastName);
	this.accounts = new ArrayList<>();
    }

    public void addAccount(Account account) throws NullPointerException {
	if (account != null) {
	    this.accounts.add(account);
	} else {
	    throw new NullPointerException("account is null.");
	}
    }
}
