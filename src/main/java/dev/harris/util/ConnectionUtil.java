package dev.harris.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;

public class ConnectionUtil {
    private static Logger logger = Logger.getRootLogger();

    public static Connection connection;

    /*
     * public static Connection getHardCodedConnection() throws SQLException { final
     * String url =
     * "jdbc:oracle:thin:@//christian-harris-oracledb-revature.cbpvvbpqtrp8.us-east-2.rds.amazonaws.com:1521/ORCL";
     * final String username = "admin"; final String password = "FirePrincess1.0";
     * if ((connection == null) || connection.isClosed()) { connection =
     * DriverManager.getConnection(url, username, password); } return connection; }
     */

    public static Connection getConnection() throws SQLException {
	logger.info("Retrieving database connection.");
	final String url = System.getenv("AWS_DB_URL"); // best not to hard code in your db credentials !
	final String username = System.getenv("AWS_DB_USERNAME");
	final String password = System.getenv("AWS_DB_PASSWORD");
	if ((connection == null) || connection.isClosed()) {
	    connection = DriverManager.getConnection(url, username, password);
	}
	return connection;
    }
}
