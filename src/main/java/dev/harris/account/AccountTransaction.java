package dev.harris.account;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class AccountTransaction {
    private String accountNumber;
    private LocalDateTime dateTime;
    private String description;
    private BigDecimal amount;
    private BigDecimal balance;

    public AccountTransaction(String accountNumber, String description, BigDecimal amount, BigDecimal balance)
	    throws NullPointerException {
	this.dateTime = LocalDateTime.now();
	if (accountNumber != null) {
	    this.accountNumber = accountNumber;
	} else {
	    throw new NullPointerException("accountNumber is null.");
	}
	if (description != null) {
	    this.description = description;
	} else {
	    this.description = "";
	}
	if (amount != null) {
	    this.amount = amount;
	} else {
	    throw new NullPointerException("amount is null.");
	}
	if (balance != null) {
	    this.balance = balance;
	} else {
	    throw new NullPointerException("balance is null.");
	}
    }

    public AccountTransaction(LocalDateTime dateTime, String accountNumber, String description, BigDecimal amount,
	    BigDecimal balance) throws NullPointerException {
	this(accountNumber, description, amount, balance);
	if (dateTime != null) {
	    this.dateTime = dateTime;
	} else {
	    throw new NullPointerException("dateTime is null.");
	}
    }

    public String getAccountNumber() {
	return this.accountNumber;
    }

    public LocalDateTime getDateTime() {
	return this.dateTime;
    }

    public String getDescription() {
	return this.description;
    }

    public BigDecimal getAmount() {
	return this.amount;
    }

    public BigDecimal getBalance() {
	return this.balance;
    }
}
