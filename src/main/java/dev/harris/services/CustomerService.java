package dev.harris.services;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

import org.apache.log4j.Logger;

import dev.harris.account.Account;
import dev.harris.account.AccountTransaction;
import dev.harris.dao.AccountDao;
import dev.harris.dao.ApplicationDao;
import dev.harris.system.Main;

public class CustomerService {

    private static Logger logger = Logger.getRootLogger();

    private final Scanner scanner = new Scanner(System.in);

    public void customerMenu() {
	logger.info("Rendering customer menu.");
	String selection = "";
	do {
	    System.out.print(
		    "1) View Accounts\n2) Withdraw funds\n3) Deposit funds\n4) Apply for new account\n0) Logout\n>");
	    selection = this.scanner.nextLine().trim();
	    switch (selection) {
	    case ("1"):
		this.viewAccounts();
		break;
	    case ("2"):
		this.withdrawFunds();
		break;
	    case ("3"):
		this.depositFunds();
		break;
	    case ("4"):
		this.applyForNewAccount();
		break;
	    case ("0"):
		// logout.
		return;
	    default:
		System.out.println(selection + " is not a valid selection.");
	    }
	} while (!selection.equals("0"));
    }

    private void viewAccounts() {
	logger.info("Viewing accounts.");
	final AccountDao accountDao = new AccountDao();
	final ArrayList<Account> accounts = accountDao.getAccounts(Main.getCurrentUser());
	String selection = "";
	int integerSelection = -1;
	do {
	    System.out.println("   Account Number\tBalance");
	    for (int i = 1; i <= accounts.size(); i++) {
		System.out.println(i + ") " + accounts.get(i - 1).printAccountSummary());
	    }
	    System.out.println("0) Back");
	    System.out.print("Select an account to view transaction history.\n>");
	    selection = this.scanner.nextLine();
	    try {
		integerSelection = Integer.parseInt(selection);
		if ((integerSelection >= 0) && (integerSelection <= accounts.size())) {
		    if (integerSelection != 0) {
			this.viewTransactionHistory(accounts.get(integerSelection - 1));
		    }
		} else {
		    System.out.println(selection + " is not a valid selection.");
		}
	    } catch (final NumberFormatException ex) {
		System.out.println(selection + "is not a valid selection.");
	    }
	} while (integerSelection != 0);
    }

    private void viewTransactionHistory(Account account) {
	logger.info("Viewing transaction hsitory.");
	String selection;
	System.out.println("Transaction history for account " + account.getAccountNumber());
	System.out.printf("%-22s%-22s%-50s%-22s%-22s\n", "Date", "Time", "Description", "Amount", "Balance");
	for (final AccountTransaction accountTransaction : account.getTransactionHistory()) {
	    final String date = accountTransaction.getDateTime().toLocalDate().toString();
	    final String time = accountTransaction.getDateTime().toLocalTime().toString();
	    final String description = accountTransaction.getDescription();
	    final String amount = accountTransaction.getAmount().toString();
	    final String balance = accountTransaction.getBalance().toString();

	    System.out.printf("%-22s%-22s%-50s%-22s%-22s\n", date, time, description, amount, balance);
	}
	do {
	    System.out.println("0) Back\n>");
	    selection = this.scanner.nextLine().trim();
	} while (!selection.equals("0"));
    }

    private void applyForNewAccount() {
	logger.info("Applying for new account.");
	final ApplicationDao applicationDao = new ApplicationDao();
	String selection = "";
	BigDecimal amount = null;
	boolean selectionIsValid = false;
	do {
	    System.out.print("Please enter your desired starting balance.\nE) Exit\n>$");
	    selection = this.scanner.nextLine().trim();
	    if (selection.equalsIgnoreCase("E")) {
		return;
	    } else {
		try {
		    amount = new BigDecimal(selection);
		    selectionIsValid = true;
		    applicationDao.addApplication(Main.getCurrentUser(), amount);
		} catch (final NumberFormatException ex) {
		    System.out.println(selection + " is not a valid dollar amount.");
		}
	    }
	} while (!selectionIsValid);
    }

    private void withdrawFunds() {
	logger.info("Withdrawing funds.");
	int accountSelection;
	String amountInput = "";
	BigDecimal amount;
	final AccountDao accountDao = new AccountDao();
	ArrayList<Account> accounts;
	while (true) {
	    accounts = accountDao.getAccounts(Main.getCurrentUser());
	    System.out.println("   Account Number\tBalance");
	    for (int i = 1; i <= accounts.size(); i++) {
		System.out.println(i + ") " + accounts.get(i - 1).printAccountSummary());
	    }
	    System.out.println("0) Back");
	    System.out.print("Select an account to withdraw funds from.\n>");
	    if (this.scanner.hasNextInt()) {
		final String temp = this.scanner.nextLine();
		accountSelection = Integer.parseInt(temp);
		if (accountSelection == 0) {
		    return;
		} else if ((accountSelection < 1) || (accountSelection > accounts.size())) {
		    System.out.println(accountSelection + " is not a valid input.");
		} else {
		    System.out.print("Enter a dollar amount to withdraw.\n$>");
		    try {
			amountInput = this.scanner.nextLine().trim();
			amount = new BigDecimal(amountInput);
			if (accounts.get(accountSelection - 1).getBalance().compareTo(amount) > 0) {
			    if (amount.compareTo(new BigDecimal("0")) > 0) {
				try {
				    System.out.println("You want to withdraw $" + amount.toString() + " from account "
					    + accounts.get(accountSelection - 1).getAccountNumber() + " with balance "
					    + accounts.get(accountSelection - 1).getBalance().toString());
				    System.out.print("Y/N\n>");
				    final String yesOrNo = this.scanner.nextLine();
				    if (yesOrNo.equalsIgnoreCase("Y") || yesOrNo.equalsIgnoreCase("YES")) {
					System.out.println("Withdrawing funds...");
					accountDao.withdrawFunds(accounts.get(accountSelection - 1).getAccountNumber(),
						amount);
					System.out.println("Funds withdrawn.");
					return;
				    } else {
					continue;
				    }
				} catch (final SQLException ex) {
				    System.out.println("Database error withdrawl failed.");
				}
			    } else {
				System.out.println("You cannot withdraw a negative amount.");
			    }
			} else {
			    System.out.println("Chosen account has insufficient funds.");
			}
		    } catch (final NumberFormatException ex) {
			System.out.println(amountInput + " is not a valid dollar amount.");
		    }
		}
	    } else {
		System.out.println(this.scanner.nextLine().trim() + " is not a valid input.");
	    }
	}
    }

    private void depositFunds() {
	logger.info("Depositing funds.");
	int accountSelection;
	String amountInput = "";
	BigDecimal amount;
	final AccountDao accountDao = new AccountDao();
	ArrayList<Account> accounts;
	while (true) {
	    accounts = accountDao.getAccounts(Main.getCurrentUser());
	    System.out.println("   Account Number\tBalance");
	    for (int i = 1; i <= accounts.size(); i++) {
		System.out.println(i + ") " + accounts.get(i - 1).printAccountSummary());
	    }
	    System.out.println("0) Back");
	    System.out.print("Select an account to deposit funds into.\n>");
	    if (this.scanner.hasNextInt()) {
		final String temp = this.scanner.nextLine();
		accountSelection = Integer.parseInt(temp);
		if (accountSelection == 0) {
		    return;
		} else if ((accountSelection < 1) || (accountSelection > accounts.size())) {
		    System.out.println(accountSelection + " is not a valid input.");
		} else {
		    System.out.print("Enter a dollar amount to deposit.\n$>");
		    try {
			amountInput = this.scanner.nextLine().trim();
			amount = new BigDecimal(amountInput);
			if (amount.compareTo(new BigDecimal("0")) > 0) {
			    try {
				System.out.println("You want to deposit $" + amount.toString() + " to account "
					+ accounts.get(accountSelection - 1).getAccountNumber() + " with balance "
					+ accounts.get(accountSelection - 1).getBalance().toString());
				System.out.print("Y/N\n>");
				final String yesOrNo = this.scanner.nextLine();
				if (yesOrNo.equalsIgnoreCase("Y") || yesOrNo.equalsIgnoreCase("YES")) {
				    System.out.println("Depositing funds...");
				    accountDao.depositFunds(accounts.get(accountSelection - 1).getAccountNumber(),
					    amount);
				    System.out.println("Funds deposited.");
				    return;
				} else {
				    continue;
				}
			    } catch (final SQLException ex) {
				System.out.println("Database error deposit failed.");
			    }
			} else {
			    System.out.println("You cannot deposit a negative amount.");
			}
		    } catch (final NumberFormatException ex) {
			System.out.println(amountInput + " is not a valid dollar amount.");
		    }
		}
	    } else {
		System.out.println(this.scanner.nextLine().trim() + " is not a valid input.");
	    }
	}
    }
}
