package dev.harris.dao;

import dev.harris.user.Customer;

public interface UserDaoInterface {

    public String getUserType(String username);

    public Customer getCustomer(String username);
}
