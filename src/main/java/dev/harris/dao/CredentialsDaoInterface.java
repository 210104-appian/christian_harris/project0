package dev.harris.dao;

public interface CredentialsDaoInterface {

    public boolean validateCredentials(String username, String password);
}
