package dev.harris.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import dev.harris.account.Account;
import dev.harris.account.Application;
import dev.harris.account.ApplicationStatus;
import dev.harris.util.AccountNumberUtil;
import dev.harris.util.ConnectionUtil;

public class ApplicationDao implements ApplicationDaoInterface {

    private static Logger logger = Logger.getRootLogger();

    @Override
    public void addApplication(String username, BigDecimal amount) {
	logger.info("Adding appliation to database.");
	final String sql = "INSERT INTO APPLICATION (USERNAME, AMOUNT, STATUS) VALUES (?, ?, 'PENDING')";
	try (Connection connection = ConnectionUtil.getConnection();
		PreparedStatement pStatement = connection.prepareStatement(sql)) {
	    pStatement.setString(1, username);
	    pStatement.setBigDecimal(2, amount);
	    pStatement.executeUpdate();
	} catch (final SQLException ex) {
	    logger.info("SQL exception thrown.");
	    ex.printStackTrace();
	}
    }

    @Override
    public ArrayList<Application> getAllPendingApplications() {
	logger.info("Getting all pending applications.");
	final ArrayList<Application> applications = new ArrayList<>();
	final String sql = "SELECT * FROM APPLICATION WHERE STATUS = 'PENDING'";
	try (Connection connection = ConnectionUtil.getConnection();
		Statement statement = connection.createStatement()) {
	    final ResultSet resultSet = statement.executeQuery(sql);
	    while (resultSet.next()) {
		final Application application = new Application(resultSet.getInt("ID"), resultSet.getString("USERNAME"),
			resultSet.getBigDecimal("AMOUNT"),
			ApplicationStatus.toApplicationStatus(resultSet.getString("STATUS")));
		applications.add(application);
	    }
	} catch (final SQLException ex) {
	    logger.info("SQL exception thrown.");
	    ex.printStackTrace();
	}
	return applications;
    }

    @Override
    public void acceptApplication(Application application) throws SQLException {
	logger.info("Accepting application ID = " + application.getId());
	final String sqlUpdateApplication = "UPDATE APPLICATION SET STATUS = 'ACCEPTED' WHERE ID = ?";

	final Account account = new Account(AccountNumberUtil.generateAccountNumber(), application.getAmount());
	final String sqlAddAccount = "INSERT INTO ACCOUNT VALUES (?, ?, ?)";

	Connection connection = null;
	PreparedStatement updateApplicationStatement = null;
	PreparedStatement addAccountStatement = null;
	try {
	    connection = ConnectionUtil.getConnection();

	    updateApplicationStatement = connection.prepareStatement(sqlUpdateApplication);
	    updateApplicationStatement.setInt(1, application.getId());

	    addAccountStatement = connection.prepareStatement(sqlAddAccount);
	    addAccountStatement.setString(1, account.getAccountNumber());
	    addAccountStatement.setBigDecimal(2, account.getBalance());
	    addAccountStatement.setDate(3, Date.valueOf(account.getDateCreated()));

	    connection.setAutoCommit(false);
	    updateApplicationStatement.executeUpdate();
	    addAccountStatement.executeUpdate();
	    connection.commit();
	    logger.info("Committed application and new account to database.");
	} catch (final SQLException ex) {
	    logger.info("SQL exception thrown.");
	    connection.rollback();
	    logger.info("Database rollback.");
	    ex.printStackTrace();
	} finally {
	    connection.close();
	    updateApplicationStatement.close();
	    addAccountStatement.close();
	}
    }

    @Override
    public void rejectApplication(Application application) {
	logger.info("Rejecting application ID = " + application.getId());
	final String sql = "UPDATE APPLICATION SET STATUS = 'REJECTED' WHERE ID = ?";
	try (Connection connection = ConnectionUtil.getConnection();
		PreparedStatement pStatement = connection.prepareStatement(sql)) {
	    pStatement.setInt(1, application.getId());
	    pStatement.executeQuery();
	} catch (final SQLException ex) {
	    logger.info("SQL exception thrown.");
	    ex.printStackTrace();
	}
    }
}
