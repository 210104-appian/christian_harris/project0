package dev.harris.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import dev.harris.account.Account;
import dev.harris.account.AccountTransaction;
import dev.harris.util.ConnectionUtil;

public class AccountDao implements AccountDaoInterface {

    private static Logger logger = Logger.getRootLogger();

    public ArrayList<Account> getAllAccounts() {
	logger.info("Getting all accounts.");
	final ArrayList<Account> accounts = new ArrayList<>();
	final String sql = "SELECT ACCOUNT_NUMBER, BALANCE, DATE_CREATED FROM ACCOUNT";
	try (Connection connection = ConnectionUtil.getConnection();
		Statement statement = connection.createStatement()) {
	    final ResultSet resultSet = statement.executeQuery(sql);
	    while (resultSet.next()) {
		accounts.add(new Account(resultSet.getString("ACCOUNT_NUMBER"), resultSet.getBigDecimal("BALANCE"),
			resultSet.getDate("DATE_CREATED").toLocalDate(),
			this.getAccountTransactions(resultSet.getString("ACCOUNT_NUMBER"), connection)));
	    }
	} catch (final SQLException ex) {
	    logger.info("SQL Exception thrown.");
	    ex.printStackTrace();
	}
	return accounts;
    }

    @Override
    public ArrayList<Account> getAccounts(String username) {
	logger.info("Getting accounnts for user: " + username);
	final ArrayList<Account> accounts = new ArrayList<>();
	final String sql = "SELECT ACCOUNT.ACCOUNT_NUMBER, ACCOUNT.BALANCE, ACCOUNT.DATE_CREATED FROM ACCOUNT JOIN USR_ACCOUNT ON ACCOUNT.ACCOUNT_NUMBER=USR_ACCOUNT.ACCOUNT_NUMBER WHERE USR_ACCOUNT.USERNAME = ?";
	try (Connection connection = ConnectionUtil.getConnection();
		PreparedStatement pStatement = connection.prepareStatement(sql)) {
	    pStatement.setString(1, username);
	    final ResultSet resultSet = pStatement.executeQuery();
	    while (resultSet.next()) {
		accounts.add(new Account(resultSet.getString("ACCOUNT_NUMBER"), resultSet.getBigDecimal("BALANCE"),
			resultSet.getDate("DATE_CREATED").toLocalDate(),
			this.getAccountTransactions(resultSet.getString("ACCOUNT_NUMBER"), connection)));
	    }
	} catch (final SQLException ex) {
	    logger.info("SQL exception thrown.");
	    ex.printStackTrace();
	}
	return accounts;
    }

    private ArrayList<AccountTransaction> getAccountTransactions(String accountNumber, Connection connection) {
	logger.info("Getting all transactions for account: " + accountNumber);
	final String sql = "SELECT * FROM ACCOUNT_TRANSACTION WHERE ACCOUNT_NUMBER = ? ORDER BY ID";
	final ArrayList<AccountTransaction> transactionHistory = new ArrayList<>();
	try (PreparedStatement pStatement = connection.prepareStatement(sql)) {
	    pStatement.setString(1, accountNumber);
	    final ResultSet resultSet = pStatement.executeQuery();
	    while (resultSet.next()) {
		transactionHistory
			.add(new AccountTransaction(resultSet.getTimestamp("TRANSACTION_TIMESTAMP").toLocalDateTime(),
				resultSet.getString("ACCOUNT_NUMBER"), resultSet.getString("DESCRIPTION"),
				resultSet.getBigDecimal("AMOUNT"), resultSet.getBigDecimal("BALANCE")));
	    }
	} catch (final SQLException ex) {
	    logger.info("SQL exception thrown.");
	    ex.printStackTrace();
	}
	return transactionHistory;
    }

    @Override
    public boolean accountExists(String accountNumber) {
	logger.info("Determining if account number " + accountNumber + " exists.");
	final String sql = "SELECT * FROM ACCOUNT WHERE ACCOUNT_NUMBER = ?";
	try (Connection connection = ConnectionUtil.getConnection();
		PreparedStatement pStatement = connection.prepareStatement(sql)) {
	    pStatement.setString(1, accountNumber);
	    final ResultSet resultSet = pStatement.executeQuery();
	    if (resultSet.next()) {
		if (resultSet.getString("ACCOUNT_NUMBER").equals(accountNumber)) {
		    return true;
		}
	    }
	} catch (final SQLException ex) {
	    logger.info("SQL exception thrown.");
	    ex.printStackTrace();
	}
	return false;
    }

    @Override
    public BigDecimal getAccountBalance(String accountNumber) {
	logger.info("Getting balance for account: " + accountNumber);
	final String sql = "SELECT BALANCE FROM ACCOUNT WHERE ACCOUNT_NUMBER = ?";
	try (Connection connection = ConnectionUtil.getConnection();
		PreparedStatement pStatement = connection.prepareStatement(sql)) {
	    pStatement.setString(1, accountNumber);
	    final ResultSet resultSet = pStatement.executeQuery();
	    if (resultSet.next()) {
		return resultSet.getBigDecimal("BALANCE");
	    }
	} catch (final SQLException ex) {
	    logger.info("SQL exception thrown.");
	    ex.printStackTrace();
	}
	return null;
    }

    @Override
    public void transferFunds(String accountFrom, String accountTo, BigDecimal amount) throws SQLException {
	logger.info("Transfering $" + amount.toString() + " from account " + accountFrom + " to account " + accountTo);
	final String sqlRemoveFunds = "UPDATE ACCOUNT SET BALANCE = BALANCE - ? WHERE ACCOUNT_NUMBER = ?";
	final String sqlDepositFunds = "UPDATE ACCOUNT SET BALANCE = BALANCE + ? WHERE ACCOUNT_NUMBER = ?";
	final AccountTransaction transactionFrom = new AccountTransaction(accountFrom,
		"Transfered $" + amount.toString() + " to account " + accountTo, amount,
		this.getAccountBalance(accountFrom).subtract(amount));
	final AccountTransaction transactionTo = new AccountTransaction(accountTo,
		"Transfered $" + amount.toString() + " from account " + accountFrom, amount,
		this.getAccountBalance(accountTo).add(amount));
	final String sqlInsertAccountTransaction = "INSERT INTO ACCOUNT_TRANSACTION (ACCOUNT_NUMBER, TRANSACTION_TIMESTAMP, DESCRIPTION, AMOUNT, BALANCE) VALUES (?, ?, ?, ?, ?)";
	Connection connection = null;
	PreparedStatement psRemoveFunds = null;
	PreparedStatement psDepositFunds = null;
	PreparedStatement psInsertAccountTransactionRemove = null;
	PreparedStatement psInsertAccountTransactionDeposit = null;
	try {
	    connection = ConnectionUtil.getConnection();

	    psRemoveFunds = connection.prepareStatement(sqlRemoveFunds);
	    psRemoveFunds.setBigDecimal(1, amount);
	    psRemoveFunds.setString(2, accountFrom);

	    psDepositFunds = connection.prepareStatement(sqlDepositFunds);
	    psDepositFunds.setBigDecimal(1, amount);
	    psDepositFunds.setString(2, accountTo);

	    psInsertAccountTransactionRemove = connection.prepareStatement(sqlInsertAccountTransaction);
	    psInsertAccountTransactionRemove.setString(1, transactionFrom.getAccountNumber());
	    psInsertAccountTransactionRemove.setTimestamp(2, Timestamp.valueOf(transactionFrom.getDateTime()));
	    psInsertAccountTransactionRemove.setString(3, transactionFrom.getDescription());
	    psInsertAccountTransactionRemove.setBigDecimal(4, transactionFrom.getAmount().negate());
	    psInsertAccountTransactionRemove.setBigDecimal(5, transactionFrom.getBalance());

	    psInsertAccountTransactionDeposit = connection.prepareStatement(sqlInsertAccountTransaction);
	    psInsertAccountTransactionDeposit.setString(1, transactionTo.getAccountNumber());
	    psInsertAccountTransactionDeposit.setTimestamp(2, Timestamp.valueOf(transactionTo.getDateTime()));
	    psInsertAccountTransactionDeposit.setString(3, transactionTo.getDescription());
	    psInsertAccountTransactionDeposit.setBigDecimal(4, transactionTo.getAmount());
	    psInsertAccountTransactionDeposit.setBigDecimal(5, transactionTo.getBalance());

	    connection.setAutoCommit(false);
	    psRemoveFunds.executeUpdate();
	    psDepositFunds.executeUpdate();
	    psInsertAccountTransactionRemove.executeUpdate();
	    psInsertAccountTransactionDeposit.executeUpdate();
	    connection.commit();
	    logger.info("Committed money transfer transaction to database.");
	} catch (final SQLException ex) {
	    logger.info("SQL exception thrown.");
	    connection.rollback();
	    logger.info("Database rollback.");
	    ex.printStackTrace();
	} finally {
	    if (connection != null) {
		connection.close();
	    }
	}
    }

    @Override
    public boolean ownsAccount(String accountNumber, String username) {
	logger.info("Determining if " + username + " owns account " + accountNumber + ".");
	final String sql = "SELECT ACCOUNT.ACCOUNT_NUMBER FROM USR JOIN USR_ACCOUNT ON USR.USERNAME=USR_ACCOUNT.USERNAME JOIN ACCOUNT ON USR_ACCOUNT.ACCOUNT_NUMBER=ACCOUNT.ACCOUNT_NUMBER WHERE USR.USERNAME = ?";
	try (final Connection connection = ConnectionUtil.getConnection();
		PreparedStatement pStatement = connection.prepareStatement(sql)) {
	    pStatement.setString(1, username);
	    final ResultSet resultSet = pStatement.executeQuery();
	    while (resultSet.next()) {
		if (resultSet.getString("ACCOUNT_NUMBER").equals(accountNumber)) {
		    return true;
		}
	    }
	} catch (final SQLException ex) {
	    logger.info("SQL exception thrown.");
	    ex.printStackTrace();
	}
	return false;
    }

    @Override
    public void withdrawFunds(String accountNumber, BigDecimal amount) throws SQLException {
	logger.info("Withdrawing $" + amount.toString() + " from account " + accountNumber + ".");
	final String sqlWithdraw = "UPDATE ACCOUNT SET BALANCE = BALANCE - ? WHERE ACCOUNT_NUMBER = ?";
	final AccountTransaction transactionWithdraw = new AccountTransaction(accountNumber,
		"Withdrew $" + amount.toString(), amount, this.getAccountBalance(accountNumber).subtract(amount));
	final String sqlInsertAccountTransaction = "INSERT INTO ACCOUNT_TRANSACTION (ACCOUNT_NUMBER, TRANSACTION_TIMESTAMP, DESCRIPTION, AMOUNT, BALANCE) VALUES (?, ?, ?, ?, ?)";

	Connection connection = null;
	PreparedStatement withdrawStatement = null;
	PreparedStatement withdrawTransaction = null;

	try {
	    connection = ConnectionUtil.getConnection();

	    withdrawStatement = connection.prepareStatement(sqlWithdraw);
	    withdrawStatement.setString(2, accountNumber);
	    withdrawStatement.setBigDecimal(1, amount);

	    withdrawTransaction = connection.prepareStatement(sqlInsertAccountTransaction);
	    withdrawTransaction.setString(1, transactionWithdraw.getAccountNumber());
	    withdrawTransaction.setTimestamp(2, Timestamp.valueOf(transactionWithdraw.getDateTime()));
	    withdrawTransaction.setString(3, transactionWithdraw.getDescription());
	    withdrawTransaction.setBigDecimal(4, transactionWithdraw.getAmount().negate());
	    withdrawTransaction.setBigDecimal(5, transactionWithdraw.getBalance());

	    connection.setAutoCommit(false);
	    withdrawStatement.executeUpdate();
	    withdrawTransaction.executeUpdate();
	    connection.commit();
	    logger.info("Committed withdrawl.");
	} catch (final SQLException ex) {
	    logger.info("SQL exception thrown.");
	    connection.rollback();
	    logger.info("Database rollback.");
	    ex.printStackTrace();
	} finally {
	    if (connection != null) {
		connection.close();
		logger.info("Closed connection to database.");
	    }
	}
    }

    @Override
    public void depositFunds(String accountNumber, BigDecimal amount) throws SQLException {
	logger.info("Depositing $" + amount.toString() + " to account " + accountNumber + ".");
	final String sqlDeposit = "UPDATE ACCOUNT SET BALANCE = BALANCE + ? WHERE ACCOUNT_NUMBER = ?";
	final AccountTransaction transactionDeposit = new AccountTransaction(accountNumber,
		"Deposited $" + amount.toString(), amount, this.getAccountBalance(accountNumber).add(amount));
	final String sqlInsertAccountTransaction = "INSERT INTO ACCOUNT_TRANSACTION (ACCOUNT_NUMBER, TRANSACTION_TIMESTAMP, DESCRIPTION, AMOUNT, BALANCE) VALUES (?, ?, ?, ?, ?)";

	Connection connection = null;
	PreparedStatement depositStatement = null;
	PreparedStatement depositTransaction = null;

	try {
	    connection = ConnectionUtil.getConnection();

	    depositStatement = connection.prepareStatement(sqlDeposit);
	    depositStatement.setString(2, accountNumber);
	    depositStatement.setBigDecimal(1, amount);

	    depositTransaction = connection.prepareStatement(sqlInsertAccountTransaction);
	    depositTransaction.setString(1, transactionDeposit.getAccountNumber());
	    depositTransaction.setTimestamp(2, Timestamp.valueOf(transactionDeposit.getDateTime()));
	    depositTransaction.setString(3, transactionDeposit.getDescription());
	    depositTransaction.setBigDecimal(4, transactionDeposit.getAmount());
	    depositTransaction.setBigDecimal(5, transactionDeposit.getBalance());

	    connection.setAutoCommit(false);
	    depositStatement.executeUpdate();
	    depositTransaction.executeUpdate();
	    connection.commit();
	    logger.info("Committed deposit.");
	} catch (final SQLException ex) {
	    logger.info("SQL exception thrown.");
	    connection.rollback();
	    logger.info("Database rollback.");
	    ex.printStackTrace();
	} finally {
	    if (connection != null) {
		connection.close();
		logger.info("Closed connection to database.");
	    }
	}
    }
}
